from os.path import dirname, join, abspath
from pyrep import PyRep
from pyrep.objects.shape import Shape
from sim_framework.envs.drone import Drone
import time
import numpy as np
from pyrep.objects.vision_sensor import VisionSensor
from skimage.color import rgb2gray

# accessing the coppelia environment
SCENE_FILE = join(dirname(abspath(__file__))+'/../scene_pla/plataformas.ttt')

class ReacherEnv(object):

    def __init__(self):

        # Setting Pyrep
        self.pr = PyRep()
        self.pr.launch(SCENE_FILE, headless=True)
        self.pr.start()
        self.agent = Drone(count=0, name="Quadricopter", num_joints=4)
        self.agent.set_control_loop_enabled(False)
        self.agent.set_motor_locked_at_zero_velocity(False) ## When the force is set to zero, it locks the motor to prevent drifting
        self.pr.step()
        self.target = Shape('Quadricopter_target')
        self.platform = Shape('Base_suspensa2')

        self.dataset_floor = [[], []]
        self.dataset_front = [[], []]

    def get_frames(self, cam):
        vision_sensor = VisionSensor(cam)
        frame = vision_sensor.capture_rgb()
        frame = np.uint8(frame*256)
        frame = rgb2gray(frame)
        if cam == 'vision_floor':
            self.dataset_floor[0]= self.dataset_floor[1]
            self.dataset_floor[1] = frame
            return self.dataset_floor
        elif cam == 'vision_front':
            self.dataset_front[0] = self.dataset_front[1]
            self.dataset_front[1] = frame
            return self.dataset_front
        ### TEST THE NEW TYPE
        # if len(dataset_front) > 1 or len(dataset_floor) > 2:
        #     dataset_front.clear()
        #     dataset_floor.clear()
        # if cam == 'vision_floor':
        #     dataset_floor.append(frame)
        # elif cam == 'vision_front':
        #     dataset_front.append(frame)

    def desloc_drone(self):
        self.desloc=self.platform.get_position()-self.agent.get_position()
        self.passo = self.desloc /4
        for passo in range(0, 4):
            self.agent.set_position(self.agent.get_position()+self.passo)
            self.pr.step()
            env.get_frames('vision_floor')
            env.get_frames('vision_front')


    def shutdown(self):
        self.pr.stop()
        self.pr.shutdown()

env = ReacherEnv()
print(len(env.dataset_front),len(env.dataset_floor))
env.desloc_drone()
print(len(env.dataset_front),len(env.dataset_floor))
np.save('Dataset_floor', env.dataset_floor)
np.save('Dataset_front', env.dataset_front)
env.shutdown()
