# create some data
import random as RND
fnx = lambda: RND.randint(0, 10)
data = [ (fnx(), fnx()) for _ in range(10) ]
target = (2, 4)

import math
def euclid_dist(v1, v2):
    x1, y1 = v1
    x2, y2 = v2
    return math.sqrt((x2 - x1)**2 + (y2 - y1)**2)

#data.sort(key=euclid_dist)

from functools import partial

p_euclid_dist = partial(euclid_dist, target)

print(p_euclid_dist((3, 3)))