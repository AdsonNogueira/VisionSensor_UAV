'''
Created on 4 de dez de 2020
@author: Adson Nogueira Alves
Contato: adsonnalves@gmail.com
Tel: (31) 98408-9129
'''
import numpy as np
import matplotlib.pyplot as plt


img_floor = np.load('Dataset_floor.npy')
img_front = np.load('Dataset_front.npy')
print('Size Datasets {} floor and {} front'.format(len(img_floor),len(img_front)))
print(img_floor.shape)
print(img_front.shape)
while 1:
    point= np.random.randint(0,2)
    print('Point choise {}'.format(point))
    f=plt.figure()
    f.add_subplot(1,2,1)
    plt.title('Floor')
    plt.imshow(img_floor[point])
    f.add_subplot(1,2,2)
    plt.title('Front')
    plt.imshow(img_front[point])
    plt.show()

# #show per frame using matplotlib
#             # plt.imshow(img,cmap='gray',interpolation = 'bicubic')
#             # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
#             # plt.show()
#             #
#             Plot realtime using opencv
#             cv2.imshow('image',img)
#             if cv2.waitKey(1) & 0xFF == ord('q'):
#                break
#
# #If use cv2 need uncommented
# cv2.destroyAllWindows()
